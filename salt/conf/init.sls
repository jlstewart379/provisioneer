{% set home = salt['environ.get']('HOME', '') %}

/etc/salt/minion.d/salt.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://conf/salt.conf

{{ home }}/.vimrc:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://conf/vimrc.conf
