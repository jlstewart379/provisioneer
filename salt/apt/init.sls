apt-transport-https:
  pkg.latest

ca-certificates:
  pkg.latest

curl:
  pkg.latest

default-jdk:
  pkg.latest

gimp:
  pkg.latest
    
git:
  pkg.latest
    
vim:
  pkg.latest
    
python3:
  pkg.latest
    
python3-dev:
  pkg.latest
    
python3-pip:
  pkg.latest

rubygems:
  pkg.latest

software-properties-common:
  pkg.latest

vagrant:
  pkg.latest
    
virtualbox:
  pkg.latest
    
virtualenv:
  pkg.latest
