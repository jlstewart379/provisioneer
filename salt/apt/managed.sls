{% set codename = salt['grains.get']('oscodename', '') %}

/etc/apt/sources.list.d/docker.list:
  file.managed:
    - contents: deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ codename }} stable
    - user: root
    - group: root
    - mode: 644 

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C:
    cmd.run:
        - unless: 'apt-key list | grep Launchpad'
        - order: first

docker-ce:
  pkg.latest:
  - require: 
    - file: /etc/apt/sources.list.d/docker.list 

docker:
  pkg.removed
