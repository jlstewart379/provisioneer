FROM debian:jessie

# Give the container. Then salt can uniquely identify.
ARG id


RUN apt-get update
RUN apt-get -y install software-properties-common git wget

RUN wget -q -O- "http://debian.saltstack.com/debian-salt-team-joehealy.gpg.key" | apt-key add -
RUN echo "deb http://debian.saltstack.com/debian jessie-saltstack main" >> /etc/apt/sources.list.d/salt.list
RUN apt-get update && apt-get install -y salt-minion

# install git

# Download a bootstrap script to install salt
RUN git clone https://gitlab.com/jlstewart379/provisioneer.git

# Change into the cloned directory
WORKDIR provisioneer

# Add the salt config files to the default location for salt configs
# RUN mkdir /srv && cp ./salt /srv/salt

# Create directory for holding config file to override default salt minion config
RUN mkdir -p /etc/salt/minion.d

# Override the default id with the id passed into the build context
RUN echo "id: ${id}" >> /etc/salt/minion.d/salt.conf

# Run salt in masterless mode in this container 
RUN echo "file_client: local" >> /etc/salt/minion.d/salt.conf

RUN cp -r salt /srv 


# Run the high state in this container
RUN salt-call state.highstate

